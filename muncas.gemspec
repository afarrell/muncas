# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'muncas/version'

Gem::Specification.new do |spec|
  spec.name          = "muncas"
  spec.version       = Muncas::VERSION
  spec.authors       = ["Allan Farrell"]
  spec.email         = ["afarrell@mun.ca"]
  spec.description   = %q{CAS client for Memorial University}
  spec.summary       = %q{Just the basics - no fancy gatewaying etc. !}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "castanet", "~> 1.1.0.pre"
end
