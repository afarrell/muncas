require 'castanet'

module MunCAS

  class Railtie < Rails::Railtie
    initializer 'muncas.initialize' do |app|
      MunCAS::Filter.setup()
    end
  end

  class Client

    include Castanet::Client

    CAS_URL = "https://login.mun.ca/cas/"
    CAS_LOGIN_URL = "https://login.mun.ca/cas/login"
    CAS_LOGOUT_URL =  "https://login.mun.ca/cas/logout"

    def cas_url
      CAS_URL
    end

    def login_url
      CAS_LOGIN_URL
    end

    def logout_url
      CAS_LOGOUT_URL
    end

    def proxy_callback_url
      nil
    end

    def proxy_retrieval_url
      nil
    end

    def service_validate_url
      "https://login.mun.ca/cas/serviceValidate"
    end

  end

  class Filter

    cattr_reader :log, :client, :fake_user

    class << self

      def setup
        @@client = MunCAS::Client.new()
        @@log = Rails.logger
      end

      def before(controller)

        if @@fake_user
          controller.session[:cas_user] = @@fake_user
          return true
        end

        last_st = controller.session[:cas_last_valid_ticket]
        last_st_service = controller.session[:cas_last_valid_ticket_service]
        
        st = read_ticket(controller)

        # If ticket is in the url and user does a refresh
        if st && last_st && last_st == st.ticket && last_st_service == st.service
          log.debug("Re-using previously validated ticket since the ticket id and service are the same.")
          return true
        elsif last_st && controller.session[:cas_user]
          # Re-use the previous ticket to   prevent redirection to the CAS server on every request.
          log.debug "Existing local CAS session detected for #{controller.session[:cas_user]}. Previous ticket #{last_st_service} will be re-used."
          return true
        end

        if st

          st.present!

          if st.ok?
            log.info("Ticket belonging to user #{st.response.username.inspect} is VALID.")
            controller.session[:cas_user] = st.response.username.dup

            # Store the ticket in the session to avoid re-validating the same service ticket with the CAS server.
            controller.session[:cas_last_valid_ticket] = st.ticket
            controller.session[:cas_last_valid_ticket_service] = st.service

            return true
          else
            log.warn("Ticket #{st.ticket.inspect} failed validation -- check log for details")
            return false
          end
        else 
          # No service ticket was present in the request
          redirect_to_cas_for_authentication(controller)
          return false
        end

      rescue OpenSSL::SSL::SSLError => sslexception
        log.error("SSL Error: #{sslexception}")
        redirect_to_cas_for_authentication(controller)
        return false
      end

      # For testing
      def fake(username, extra_attributes = nil)
        @@fake_user = username
        @@fake_extra_attributes = extra_attributes
      end

      def login_url(controller)
        service_url = read_service_url(controller)
        url = add_service_to_login_url(service_url)
        return url
      end

      def add_service_to_login_url(service_url)
        uri = URI.parse(@@client.login_url)
        uri.query = (uri.query ? uri.query + "&" : "") + "service=#{CGI.escape(service_url)}"
        uri.to_s
      end

      def read_service_url(controller)
        controller.request.original_url.split("?").first
      end

      def redirect_to_cas_for_authentication(controller)
        redirect_url = login_url(controller)

        if controller.session[:previous_redirect_to_cas] &&
          controller.session[:previous_redirect_to_cas] > (Time.now - 1.second)
          log.warn("Previous redirect to the CAS server was less than a second ago. The client at #{controller.request.remote_ip} may be stuck in a redirection loop!")
          controller.session[:cas_validation_retry_count] ||= 0

          if controller.session[:cas_validation_retry_count] > 3
            log.error("Redirection loop intercepted. Client at #{controller.request.remote_ip} will be redirected back to login page and forced to renew authentication.")
            redirect_url += "&renew=1&redirection_loop_intercepted=1"
          end

          controller.session[:cas_validation_retry_count] += 1
        else
          controller.session[:cas_validation_retry_count] = 0
        end

        controller.session[:previous_redirect_to_cas] = Time.now

        log.debug("Redirecting to #{redirect_url.inspect}")
        controller.send(:redirect_to, redirect_url)
      end

      private

        def read_ticket(controller)

          # Get the ticket but don't pass it along to controller action
          ticket = controller.params.delete(:ticket)

          return nil unless ticket

          # log.debug("Request contains ticket #{ticket.inspect}.")

          client.service_ticket(ticket, read_service_url(controller))
        end

    end
  end

end
